// Escreva uma função chamada snapCrackle que leva um parâmetro: maxValue.
// Esta função deve fazer um loop de 1 até maxValue (inclusive) e montar uma string com as seguintes condições para cada número:
// Se o número for ímpar, no lugar dele, concatenar "Snap, " no final da string.
// Se o número for múltiplo de 5, no lugar dele, concatenar "Crackle, " no final da string.
// Se o número for tanto ímpar quanto múltiplo de 5, no lugar dele, concatenar "SnapCrackle, " no final da string.
// Se número não for nem ímpar nem múltiplo de 5, concatenar o próprio número e ", " no final da string.
// Esta função deve fazer o console.log() da string final depois de maxValue iterações do loop.
// Exemplo
// snapCrackle(12) deve mostrar a string Snap, 2, Snap, 4, SnapCrackle, 6, Snap, 8, Snap, Crackle, Snap, 12,

// Bônus (2 pontos máx)
// Escreva uma segunda função chamada snapCracklePrime com 1 regra adicional: Se o número for primo, no lugar dele, concatenar "Prime, " no final da string

// snapCracklePrime(15) deve mostrar a string Snap, Prime, SnapPrime, 4, SnapCracklePrime, 6, SnapPrime, 8, Snap, Crackle, SnapPrime, 12, SnapPrime, 14, SnapCrackle,

function snapCrackle(maxValue){
    const loop = []
    let counter = 1, mult5 = 5, synthesis = ""
 
    

    while(counter <= maxValue){


    if(counter % mult5 === 0 && counter % 2 != 0){
        loop.push("SnapCrackle, ")
    }        
    else if(counter % 2 != 0){
        loop.push("Snap, ")
    }
    else if(counter % mult5 === 0){  
        loop.push("Crackle, ")
    }
    else{
        loop.push(counter + ", ")
    }
    
    
    synthesis += loop[counter -1]
    counter++
    }
    return synthesis
}
function snapCracklePrime(maxValue){
    const loop = []
    let counter = 1, mult5 = 5, synthesis = ""
    while(counter <= maxValue){


    if(counter % mult5 === 0 && counter % 2 != 0){
        if(counter === 5){
            loop.push("SnapCracklePrime, ")
        }
        else{            
            loop.push("SnapCrackle, ")
        }
    }        
    else if(counter % 2 != 0){
        if(counter === 1){
            loop.push("snap, ")
        }
        else if( counter === 3 || counter === 5 || counter === 7 || counter === 11){
            loop.push("SnapPrime, ")
        }
        else if(counter % 2 != 0 && counter % 3 != 0 && counter % 5 != 0 && counter % 7 != 0 && counter % 11 != 0){
            loop.push("SnapPrime, ")
        }
        else{
            loop.push("Snap, ")
        }    
    }
    else if(counter % mult5 === 0){  
        loop.push("Crackle, ")  
    }
    else{
        if(counter === 2){
            loop.push("prime, ")
        }
        else{
            loop.push(counter + ", ")
        }
    }
    synthesis += loop[counter -1]
    counter++
    }
    return synthesis
}
    console.log(snapCrackle(10))
    console.log(snapCracklePrime(230))
    
